#include <config.h>
#include <gtk/gtk.h>
#include <libfoocanvas/libfoocanvas.h>

GtkWidget *create_newwin(gboolean normal, gchar *appname, gchar *title);

GtkWidget *create_canvas_primitives (void);
GtkWidget *create_canvas_arrowhead (void);
GtkWidget *create_canvas_fifteen (void);
GtkWidget *create_canvas_features (void);
GtkWidget *create_canvas_scalability (void);
