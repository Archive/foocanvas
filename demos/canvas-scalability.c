#include <config.h>
#include <math.h>
#include "canvas_demo.h"

#define N_COLS 5
#define N_ROWS 20
#define PADDING 10

#define USE_PIXMAP 

GtkWidget *
create_canvas_scalability (void)
{
	GtkWidget *vbox;
	GtkWidget *frame;
	GtkWidget *canvas;
	GtkWidget *table;
	GtkWidget *w;
	GdkPixbuf *pixbuf;
	FooCanvasItem *item;
	int i, j, width, height;

	vbox = gtk_vbox_new (FALSE, 4);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 4);
	gtk_widget_show (vbox);

	table = gtk_table_new (2, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), 4);
	gtk_table_set_col_spacings (GTK_TABLE (table), 4);
	gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);
	gtk_widget_show (table);
	
	frame = gtk_frame_new (NULL);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
	gtk_table_attach (GTK_TABLE (table), frame,
			  0, 1, 0, 1,
			  GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			  GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			  0, 0);
	gtk_widget_show (frame);

	/* Create the canvas and board */

	pixbuf = gdk_pixbuf_new_from_file("toroid.png", NULL);
#ifdef USE_PIXMAP
	width = gdk_pixbuf_get_width (pixbuf) + 3;
	height = gdk_pixbuf_get_height (pixbuf) +1;
#else
	width = 37;
	height = 19;
#endif
	
	canvas = foo_canvas_new ();
	gtk_widget_set_usize (canvas, 600, 450);

	foo_canvas_set_scroll_region (FOO_CANVAS (canvas),
				      0, 0,
				      N_COLS * (width + PADDING),
				      N_ROWS * (height + PADDING));
	gtk_container_add (GTK_CONTAINER (frame), canvas);
	gtk_widget_show (canvas);

	for (i = 0; i < N_COLS; i++) {
		for (j = 0; j < N_ROWS; j++) {
#ifdef USE_PIXMAP
			item = foo_canvas_item_new (foo_canvas_root (FOO_CANVAS (canvas)),
						    foo_canvas_pixbuf_get_type (),
						    "x", (double) i * (width + PADDING),
						    "y", (double) j * (height + PADDING),
						    "pixbuf", pixbuf,
						    NULL);
#else
			item = foo_canvas_item_new (foo_canvas_root (FOO_CANVAS (canvas)),
						    foo_canvas_rect_get_type (),
						    "x1", (double) i * (width + PADDING),
						    "y1", (double) j * (height + PADDING),
						    "x2", (double) i * (width + PADDING) + width ,
						    "y2", (double) j * (height + PADDING) + height,
						    "fill_color", (j%2)?"mediumseagreen":"steelblue",
						    NULL);
#endif
			
		}
	}

	w = gtk_hscrollbar_new (GTK_LAYOUT (canvas)->hadjustment);
	gtk_table_attach (GTK_TABLE (table), w,
			  0, 1, 1, 2,
			  GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			  GTK_FILL,
			  0, 0);
	gtk_widget_show (w);

	w = gtk_vscrollbar_new (GTK_LAYOUT (canvas)->vadjustment);
	gtk_table_attach (GTK_TABLE (table), w,
			  1, 2, 0, 1,
			  GTK_FILL,
			  GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			  0, 0);
	gtk_widget_show (w);

	
	return vbox;
}
